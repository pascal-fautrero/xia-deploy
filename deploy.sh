#!/bin/bash

SOURCE_DIR=/home/casimir/Documents/xia/build

if [ -d release ];then
  rm -r release/*
else
  mkdir release
fi
CWD=$(pwd)
cd deployments

echo "======================================================================"
echo "Linux package"
echo "======================================================================"

tmp_dir=$(mktemp -d -t xia-XXXXXXXXXX)
mkdir -p $tmp_dir/xia-3/DEBIAN
mkdir -p $tmp_dir/xia-3/usr/bin
mkdir -p $tmp_dir/xia-3/usr/share/xia

cp $SOURCE_DIR/xia.py $tmp_dir/xia-3/usr/bin/xia
cp -r $SOURCE_DIR/xiaconverter $tmp_dir/xia-3/usr/bin/

cp -r $SOURCE_DIR/share/* $tmp_dir/xia-3/usr/share/xia
cp $SOURCE_DIR/xia.cnf $tmp_dir/xia-3/usr/share/xia

numVersion=$(grep numVersion $tmp_dir/xia-3/usr/share/xia/xia.cnf | awk '{print $3}')
releaseVersion=$(grep releaseVersion $tmp_dir/xia-3/usr/share/xia/xia.cnf | awk '{print $3}')

cat > $tmp_dir/xia-3/DEBIAN/control <<EOF
Package: xia
Version: $numVersion$releaseVersion
Architecture: all
Maintainer: Pascal Fautrero pascal.fautrero@gmail.com
Depends: python3, python3-tk, python3-pil
Installed-Size: 9626
Homepage: https://xia.funraiders.org
Description: XIA is a tool to convert svg files to html5 resources.
EOF

sed -i "s|share|/usr/share/xia|g" $tmp_dir/xia-3/usr/share/xia/xia.cnf
sed -i 's|xia.cnf|/usr/share/xia/xia.cnf|g' $tmp_dir/xia-3/usr/bin/xia

cd $tmp_dir
dpkg --build xia-3
cp xia-3.deb $CWD/release
cd $CWD
rm -r $tmp_dir


echo "======================================================================"
echo "Linux package INKSCAPE 1.0"
echo "======================================================================"

tmp_dir=$(mktemp -d -t xia-XXXXXXXXXX)
mkdir -p $tmp_dir/xia-3/DEBIAN
mkdir -p $tmp_dir/xia-3/usr/bin
mkdir -p $tmp_dir/xia-3/usr/share/inkscape/extensions/xia
mkdir -p $tmp_dir/xia-3/usr/share/xia

cp $SOURCE_DIR/inkscape-plugin/xia.py $tmp_dir/xia-3/usr/share/inkscape/extensions/xia/
cp $SOURCE_DIR/inkscape-plugin/xia.inx $tmp_dir/xia-3/usr/share/inkscape/extensions/xia/
cp $SOURCE_DIR/xia.cnf $tmp_dir/xia-3/usr/share/inkscape/extensions/xia/
cp -r $SOURCE_DIR/xiaconverter $tmp_dir/xia-3/usr/share/inkscape/extensions/xia/
cp -r $SOURCE_DIR/share/* $tmp_dir/xia-3/usr/share/inkscape/extensions/xia/


cp $SOURCE_DIR/xia.py $tmp_dir/xia-3/usr/bin/xia
cp -r $SOURCE_DIR/xiaconverter $tmp_dir/xia-3/usr/bin/
cp -r $SOURCE_DIR/share/* $tmp_dir/xia-3/usr/share/xia
cp $SOURCE_DIR/xia.cnf $tmp_dir/xia-3/usr/share/xia

numVersion=$(grep numVersion $tmp_dir/xia-3/usr/share/xia/xia.cnf | awk '{print $3}')
releaseVersion=$(grep releaseVersion $tmp_dir/xia-3/usr/share/xia/xia.cnf | awk '{print $3}')

cat > $tmp_dir/xia-3/DEBIAN/control <<EOF
Package: xia
Version: $numVersion$releaseVersion
Architecture: all
Maintainer: Pascal Fautrero pascal.fautrero@gmail.com
Depends: python3, python3-tk, python3-pil, inkscape
Installed-Size: 9626
Homepage: https://xia.funraiders.org
Description: XIA is a tool to convert svg files to html5 resources.
EOF

sed -i "s|share|/usr/share/inkscape/extensions/xia|g" $tmp_dir/xia-3/usr/share/inkscape/extensions/xia/xia.cnf
sed -i 's|./xia.cnf|/usr/share/inkscape/extensions/xia/xia.cnf|g' $tmp_dir/xia-3/usr/share/inkscape/extensions/xia/xia.py

sed -i "s|share|/usr/share/xia|g" $tmp_dir/xia-3/usr/share/xia/xia.cnf
sed -i 's|xia.cnf|/usr/share/xia/xia.cnf|g' $tmp_dir/xia-3/usr/bin/xia

cd $tmp_dir
dpkg --build xia-3
mv xia-3.deb xia-3-inkscape.deb
cp xia-3-inkscape.deb $CWD/release
cd $CWD

rm -r $tmp_dir


#cp -r $SOURCE_DIR/inkscape-plugin xia-linux
#tar -zcvf xia-linux.tar xia-linux
#mv xia-linux.tar ../release

echo "======================================================================"
echo "Windows package standalone"
echo "======================================================================"

cd $CWD/deployments

if [ -d xia-windows/xia ];then
  rm -r xia-windows/xia/*
fi
touch xia-windows/xia/xia.bat
echo -e "@echo off\r\npython37\App\python.exe xia.py\r\npause" > xia-windows/xia/xia.bat
cp -r $SOURCE_DIR/xiaconverter xia-windows/xia
cp -r $SOURCE_DIR/share xia-windows/xia
cp $SOURCE_DIR/xia.py xia-windows/xia
cp $SOURCE_DIR/xia.cnf xia-windows/xia
cp -r ../kit-windows/python37 xia-windows/xia
zip -r xia-windows.zip xia-windows
mv xia-windows.zip ../release

echo "======================================================================"
echo "Windows package inkscape extension"
echo "======================================================================"

cd $CWD/deployments

if [ -d xia-windows/xia ];then
  rm -r xia-windows/xia/*
fi
mkdir -p xia-windows/xia
#mkdir xia-windows/xia/lib
cp -r $SOURCE_DIR/xiaconverter xia-windows/xia
cp -r $SOURCE_DIR/share xia-windows/xia
cp $SOURCE_DIR/inkscape-plugin/xia.py xia-windows/xia
cp $SOURCE_DIR/inkscape-plugin/xia.inx xia-windows/xia
cp $SOURCE_DIR/xia.cnf xia-windows/xia
#cp -r ../kit-windows/lib/* xia-windows/xia/lib
cd xia-windows
zip -r ../xia-inkscape-windows.zip xia
cd ..
mv xia-inkscape-windows.zip ../release


echo "======================================================================"
echo "MAC OS X package"
echo "======================================================================"

cd $CWD/deployments

rm -rf xia-macos/*
rm xia-macos/.gitignore
if [ -d xia-macos/.git ];then
  rm -rf xia-macos/.git
fi
git clone https://github.com/gbarre/xia_mac_installer.git xia-macos

cp $SOURCE_DIR/inkscape-plugin/xia.py xia-macos/Xia_Installer.app/Contents/Resources/files
cp $SOURCE_DIR/inkscape-plugin/xia.inx xia-macos/Xia_Installer.app/Contents/Resources/files
cp $SOURCE_DIR/xia.cnf xia-macos/Xia_Installer.app/Contents/Resources/files
cp -r $SOURCE_DIR/xiaconverter xia-macos/Xia_Installer.app/Contents/Resources/files/
cp -r $SOURCE_DIR/share xia-macos/Xia_Installer.app/Contents/Resources/files/
rm -rf xia-macos/.git
cd xia-macos
zip -r xia-macos.zip Xia_Installer.app
cd ..
mv xia-macos/xia-macos.zip ../release

echo "======================================================================"
echo "CDN deployment"
echo "======================================================================"

cd $CWD/deployments

if [ -d xia-cdn/cdn ];then
  rm -r xia-cdn/cdn/*
fi
mkdir -p xia-cdn/cdn/xia30
for D in `find $SOURCE_DIR/share/themes -mindepth 1 -maxdepth 1 -type d`
do
    cp -r $SOURCE_DIR/share/vendors/. $D/js
    cp -r $SOURCE_DIR/share/fonts/. $D/font
done
cp -r $SOURCE_DIR/share/themes/* xia-cdn/cdn/xia30
cp -r $SOURCE_DIR/share/vendors/. xia-cdn/cdn/xia30/js
cp -r $SOURCE_DIR/share/xiaengine/. xia-cdn/cdn/xia30/js
zip -r xia-cdn.zip xia-cdn
mv xia-cdn.zip ../release
